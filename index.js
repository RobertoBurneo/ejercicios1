function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
function quitarItem() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.removeItem(clave);
  spanValor.innerText = "SSESION ELIMINADA : " + clave;
}

function limpiarItem() {
  sessionStorage.clear();
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Se limpio todos los datos de sessionStorage ";
}
function longitudItem() {
  var numElementSaved = sessionStorage.length;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Numero de elementos guardados en session: " + numElementSaved;
}
